const request = require('request');

doPost = async (path, body) => {
	return request.post({
		url: `${module.exports.baseURL}/api/mobile/report/${module.exports.sessionID}/${path}`,
		form: body
	});
}

module.exports = {
	baseURL: 'http://crashkenhmlg.eastus.cloudapp.azure.com',
	sessionID: undefined,
	report: {
		start: async (name, level) => {
			return doPost('start', {
				name: name,
				level: level ? 0 : level,
			});
		},
		end: async () => {
			return doPost('end', {});
		},
		quit: async (succeed, errorCause) => {
			return doPost('quit', {
				succeed: succeed,
				cause: errorCause
			});
		},
	},
}
