const elements = require('./elements.json');
const functional = require('./functional');
const token = require('./token');
const wd = require("wd");
const asserters = wd.asserters;

global.config = require('../config/default.json');

let driver;

function setup(token){
	driver =  wd.remote({
		hostname: global.config.crashken.host,
	  port: global.config.crashken.port,
		path: 'api/mobile/wd/hub'
	}, 'promiseChain');

	return driver.init({
		//token: token,
		apiKey: "96ec549f-6587-4133-9956-8ab77e156038",
		deviceId:  "5721493e919d21001e7bd0fa",
		executionName: 'Meu Vivo - Demo CI&IT',
		skipUnlock: true,
		//appPackage: 'br.com.vivo',
		//appActivity: 'br.com.vivo.MainActivity',

		//app: "http://"

		//notifySMS: true,
		//notifyEmail: true,
	}).then((params) => {
			return new Promise((resolve) => {
				functional.setup(params[0]);
				resolve();
			})
	})
	.setImplicitWaitTimeout(20000);
}
function login(){

	return driver.sleep(5000)
	.then(() => functional.startTestCase('Login'))
	.elementById('com.android.packageinstaller:id/permission_allow_button').click()
	.elementById('com.android.packageinstaller:id/permission_allow_button').click()
	.elementByXPath(elements.login.notYet).click()
	/*.then(() => {
		throw new Error("Demo de erro");
	})*/
	.elementByXPath(elements.login.phoneNumberField).click()
	/////////////////////////////////////////////////////////////////////////////
	//1
	.performTouchAction(new wd.TouchAction().press({x: 97, y: 759}).release())
	.performTouchAction(new wd.TouchAction().press({x: 97, y: 759}).release())
	//9
	.performTouchAction(new wd.TouchAction().press({x: 445, y: 1005}).release())
	.performTouchAction(new wd.TouchAction().press({x: 445, y: 1005}).release())
	//7
	.performTouchAction(new wd.TouchAction().press({x: 95, y: 1005}).release())
	//8
	.performTouchAction(new wd.TouchAction().press({x: 272, y: 1005}).release())
	//6
	.performTouchAction(new wd.TouchAction().press({x: 445, y: 882}).release())
	//7
	.performTouchAction(new wd.TouchAction().press({x: 95, y: 1005}).release())
	.performTouchAction(new wd.TouchAction().press({x: 95, y: 1005}).release())
	//8
	.performTouchAction(new wd.TouchAction().press({x: 272, y: 1005}).release())
	.performTouchAction(new wd.TouchAction().press({x: 272, y: 1005}).release())
	/////////////////////////////////////////////////////////////////////////////
	.elementByXPath(elements.login.enterButton).click()
	.then(() => functional.end())
}
function allowPermissions(){
	return driver.sleep(1000)
	.then(() => functional.startTestCase('Allow Permissions'))
	.elementById('com.android.packageinstaller:id/permission_allow_button').click()
	.sleep(5000)
	.elementByXPath(elements.login.accept).click()
	.then(() => functional.end())
}
function editInfo(){
	return driver.sleep(1000)
	.then(() => functional.startTestCase('Edit info'))
	.sleep(4000)
	.performTouchAction(new wd.TouchAction().press({x: 97, y: 759}).release())
	.elementByXPath(elements.main.menuButton).click()
	.performTouchAction(new wd.TouchAction().press({x: 97, y: 759}).release())
	.elementByXPath(elements.menu.personalInformationButton).click()
	.elementByXPath(elements.personalInfo.name).text().then((txt) => {
		return new Promise((resolve, reject) => {
			console.log(txt, txt.trim() == 'Nome completo: JOAO BENEDITO DE SOUZA')
			if(txt.trim() == 'Nome completo: JOAO BENEDITO DE SOUZA'){
				resolve();
			}else{
				reject();
			}
		})
	})
	.performTouchAction(new wd.TouchAction()
		.press({x: 265, y: 1025})
		.wait(500)
		.moveTo({x: 265, y: 600})
		.release())
	.elementByXPath(elements.personalInfo.editInfoButton).click()
	.performTouchAction(new wd.TouchAction()
		.press({x: 265, y: 1025})
		.wait(500)
		.moveTo({x: 265, y: 650})
		.release())
	.elementByXPath(elements.personalInfo.saveButton).click()
	.then(() => functional.end())
}

//token.get()
//.then(setup)
setup()
.then(login)
.then(allowPermissions)
.then(editInfo)
.then(() => driver.quit())
.catch( async (err) => {
	console.error(err.stack);
	await functional.quit(err.stack)
	driver.quit();
});
