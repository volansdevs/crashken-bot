const querystring = require('querystring');
const http = require('http');
const uuidv4 = require('uuid/v4');

let host = 'localhost';
let port = 4200;

let sessionId;
let endpoint;

function post(path, fields){
	return new Promise((resolve, reject) => {
		let postData = fields;

		let options = {
			hostname: host,
		 	port: port,
			path: path,
			method: 'POST',
			headers: {
		    'Content-Type': 'application/json',
		    'Content-Length': Buffer.byteLength(postData)
		  }
		};

		const req = http.request(options, (res) => {
			let output = '';
		  res.setEncoding('utf8');
		  res.on('data', (chunk) => {
				output += chunk;
		  });
		  res.on('end', () => {
				resolve(output);
		  });
		});

		req.on('error', (e) => {
			reject(e);
		});

		req.write(postData);
		req.end();
	});
}

module.exports = {
	setup: (_sessionId) => {
		sessionId = _sessionId;
		endpoint = `/api/mobile/reporter/${sessionId}/execution-group`;
	},
	startTestCase: (name) => {
		return post(endpoint + '/start', JSON.stringify({functional: {description: name, type: "suite", id: uuidv4(), children: []}}));
	},
	end: () => {
		return post(endpoint + '/end', '{}');
	},
	quit: async (cause) => {
		if(cause){
			return post(endpoint + '/quit', JSON.stringify({succeed: false, reason: cause}));
		}else{
			return post(endpoint + '/quit', JSON.stringify({succeed: true}));
		}
	}
}
