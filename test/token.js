const post = require('./post');
const querystring = require('querystring');

module.exports = {
	get: async function() {
		return new Promise((resolve, reject) => {
			post('/services/device/ticket/alloc', 'application/x-www-form-urlencoded', querystring.stringify({
					deviceId: '5721493e919d21001e7bdbb2',
					apiKey: 'e7d09b1e-d2c8-45b9-b6c4-67696186715f',
				})
			).then(async (resp) => {
				resp = JSON.parse(resp);
				while(resp.position !== 0) {
					console.log('Device in use, your queue position: '+ resp.position);

					let checkResp = await post('/services/device/ticket/check','application/x-www-form-urlencoded', querystring.stringify({
						token: resp.token,
					}));
					checkResp = JSON.parse(checkResp);

					resp.position = checkResp.position;
					await module.exports.sleep(5000);
				}
				resolve(resp.token);
			}).catch((e) => {
				console.error(e.stack);
				reject(e);
			});
		});
	},
	sleep: async (duration) => {
		return new Promise((resolve) => {
			setTimeout(resolve, duration);
		});
	}
}
