const wd = require("wd");
const crashkenUtil = require('./crashkenUtil');

let driver = wd.remote(`${crashkenUtil.baseURL}/api/mobile/wd/hub`, 'promiseChain');

console.log(crashkenUtil);

driver.init({
	apiKey: "e0f407eb-cb46-4519-86b8-8cb0b8f06589",
	deviceId:  "572146ca7014aceb15854193",
	executionName: 'Demo - Report Result',

	appPackage: 'banco.devicelab.com.br.banco',
	appActivity: '.MainActivity',
}).then(async (params) => crashkenUtil.sessionID = params[0])
.then(() => crashkenUtil.report.start('Fazer Login'))
.elementById('banco.devicelab.com.br.banco:id/main_button_login').click()
.then(() => crashkenUtil.report.end())
.then(() => crashkenUtil.report.quit(true))
.catch((e) => crashkenUtil.report.quit(false, e.stack))
.quit();
