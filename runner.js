var wd = require("wd");
const {VM} = require('vm2');
const request = require('request');

global.config = require('./config/default.json');

let startupParams = JSON.parse(process.argv[2]);

global.testSuite = startupParams.testSuite;
global.device = startupParams.device;
global.apiKey = startupParams.apiKey;
global.token = startupParams.token;
global.sessionId = startupParams.sessionId;
global.testPath = startupParams.path;

global.browser = wd.remote(`${config.crashken.protocol}://${config.crashken.host}:${config.crashken.port}/api/mobile/wd/hub`, 'promiseChain');

let sandbox = {
	setTimeout: setTimeout,
	driver: global.browser,
	device: global.device,
	request: request,
	TouchAction: wd.TouchAction,
	console: {
		log: (o) => {

			process.send({
				action: 'console',
				value: o != null ? o.toString() : 'undefined',
			});
		}
	},
	JSON: JSON,
	testSuite: {
		variables: {
			get: (key) => {
				let index = global.testSuite.variables.findIndex((variable) => variable.name == key);

				if(index >= 0){
					return global.testSuite.variables[index].value;
				}else{
					return null;
				}
			},
			set: (key, value) => {

				process.send({
					action: 'set-var',
					key: key,
					value: value
				});
				
				let index = global.testSuite.variables.findIndex((variable) => variable.name == key);

				if(index >= 0){
					global.testSuite.variables[index].value = value;
				}
			},
		}
	},
	done: () => {
		process.send({
			action: 'done'
		});
	},
	doneSync: (cbId, err, data) => {
		process.send({
			action: 'done-sync',
			err: err,
			data: data,
			id: cbId,
		});
	},
	fail: (err) => {
		process.send({
			action: 'fail',
			err: err,
		});
	},
}

let tasks = {
	initSession: () => {
		if(global.sessionId){
			return tasks.attachSession();
		} else {
			return tasks.createSession();
		}
	},
	attachSession: () => {
		return new Promise((resolve, reject) => {
			global.browser.attach(sessionId, function(err, capabilities) {

				if (err) {
					// TODO:
					console.log(new Date().toLocaleString(),'[RUNNER] - AttSession:', err);
					reject(err);
				}

				resolve();
			});
		});
	},
	createSession: () => {
		return new Promise((resolve, reject) => {
			console.log(new Date().toLocaleString(),'[RUNNER] - CreateSession:');

			let startCaps = {
				token: global.token,
				executionName: global.testSuite.name,
				executionPath: global.testPath,
				skipUnlock: true,
				fullReset: false,
				noReset: true,
				deviceId: global.device._id
			}

			let notify = global.testSuite.capabilities.find(capb => {if(capb.sendSlack) return capb});

			if(notify && notify.sendSlack){
				startCaps.notifySlack = notify.sendSlack;
			}

			if(global.device.OS.platform.toLowerCase() == 'android'){

				let appPackage = global.testSuite.capabilities.find((cap) => cap.name === 'appPackage');
				let appActivity = global.testSuite.capabilities.find((cap) => cap.name === 'appActivity');
				let app = global.testSuite.capabilities.find((cap) => cap.name === 'app');
				let url = global.testSuite.capabilities.find((cap) => cap.name === 'url');
				let fullReset = global.testSuite.capabilities.find((cap) => cap.name === 'fullReset');
				let noReset = global.testSuite.capabilities.find((cap) => cap.name === 'noReset');

				if(fullReset != undefined && fullReset != null) {
					startCaps.fullReset = (fullReset == 'true');
				}

				if(noReset != undefined && noReset != null) {
					startCaps.noReset = (noReset == 'true');
				}

				if(appPackage  && appActivity) {
					startCaps.appPackage = appPackage.value;
					startCaps.appActivity = appActivity.value;
					startCaps.automationName =  "uiautomator2";
				}else if(url) {
					startCaps.browserName = 'chrome';
				}else if(app){
					startCaps.app = app.value;
				}
			}else if(global.device.OS.platform.toLowerCase() === 'ios'){

				let bundleId = global.testSuite.capabilities.find((cap) => cap.name === 'bundleId');

				if(bundleId) {
					startCaps.bundleId = bundleId.value;
				}

			}

			console.log(new Date().toLocaleString(),'[RUNNER] - CreateSession - Caps:', startCaps);
			global.browser.init(startCaps, async function(err, args) {
				
				if (err) {
					// TODO:
					console.log(new Date().toLocaleString(),'[RUNNER] - CreateSession - err:', err);
					process.send({
						action: 'startup-fail',
						value: err.data
					});

					reject('Failed to start session');
				}

				global.sessionId = args[0];

				await global.browser.setImplicitWaitTimeout(20000);

				resolve();
			});
		});
	},
	startVM: () => {
		return new Promise((resolve, reject) => {
			global.vm = new VM({
				sandbox: sandbox,
			});

			process.send({
				action: 'started',
				value: global.sessionId,
			});
			console.log(new Date().toLocaleString(),'[RUNNER] - Starting VM');

			resolve();
		});
	}
}

Promise.resolve()
	.then(() => tasks.initSession())
	.then(() => tasks.startVM())
	.then(() => {
		process.on('message', function( data ) {
			console.log(new Date().toLocaleString(),'[RUNNER] - onMessage: ', data.action, data.value);

			switch(data.action){
				case 'quit':
					global.browser.quit()
					process.send({
						id: data.id,
						action: 'quit'
					});
					break;
				case 'run':
					try {
						let code = `(async () => {
													try{
														${data.value}
													}catch(____e){
															console.log(____e);
													}
												})();
												`;
						global.vm.run(code);
					} catch (err) {
						console.error('Failed to execute script.', err.toString());
						process.send({
							action: 'fail',
							value: err.toString()
						});
					}
					break;
			}
		});
	})
	.catch(async (e) => {
		//global.log.error('BOOTSTRAP', 'Application bootstrap failed');
		//global.log.error(e);
		console.error(e);
		process.exit(1);
	});
