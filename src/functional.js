const request = require('request');
const uuidv4 = require('uuid/v4');

global.config = require('../config/default.json');

function post(path, fields){

	return new Promise((resolve, reject) => {
		request.post({
			url: `${config.crashken.protocol}://${global.config.crashken.host}:${global.config.crashken.port}${path}`,
			form: fields
		}, function(err,httpResponse,body) {
			if(err){
				reject(err);
			}else{
				resolve(body);
			}
		});
	});
}

class Functional{

	constructor(){
		this.sessionId = null;
		this.endpoint = null;
	}

	setup(_sessionId){
		this.sessionId = _sessionId;
		this.endpoint = `/api/mobile/report/${this.sessionId}`;
	}
	async startStep(name) {
		return post(this.endpoint + '/step/start', {alias: name});
	}
	startTestCase(name){
		return post(this.endpoint + '/start', {name: name, type: "suite", id: uuidv4(), children: []});
	}
	end() {
		return post(this.endpoint + '/end', {});
	}
	quit(cause){
		if(cause){
			return post(this.endpoint + '/quit', {succeed: false, cause: cause});
		}else{
			return post(this.endpoint + '/quit', {succeed: true});
		}
	}
}

module.exports = Functional;
