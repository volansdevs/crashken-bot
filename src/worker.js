const post = require('./post');
const querystring = require('querystring');
const cluster = require('cluster');
const Functional = require('./functional');
const async = require('async');
const uuidv4 = require('uuid/v4');
const exec = require('child_process').exec;

function processScript(step, cbId, device, testSuite){
	console.log(new Date().toLocaleString(),'[WORKER] - processScript', step);

	if (step.defaultValue.indexOf('done(') == -1 && step.defaultValue.indexOf('fail(') == -1) {
		step.defaultValue += `\r\n doneSync('${cbId}')`;
	}

	/*let code = `(async () => {
							${step.defaultValue}
							})();
							`*/
	return step.defaultValue;
}

function processValidateAction(step, cbId, device, testSuite){
	console.log(new Date().toLocaleString(),'[WORKER] - processValidate', step, testSuite);

	let text = '';
	if (step.defaultValue.validation.data.type == 'static') {
		text = step.defaultValue.validation.data.value;
	} else {
		text = testSuite.variables.find((v) => v.name === step.defaultValue.validation.data.value).value;
	}

	let cmd = `let element;
	let exceptionMessage;

	try {
	`;

	switch(step.defaultValue.element.method) {
		case 'xpath':
			cmd += `	element = await driver.elementByXPath('${step.defaultValue.element.reference}');`;
		break;
		case 'resource-id':
			cmd += `	element = await driver.elementById('${step.defaultValue.element.reference}');`;
		break;
	}

	if(step.defaultValue.validation.exceptionMessage){
		cmd += `
		exceptionMessage = '${step.defaultValue.validation.exceptionMessage}';
		`;
	}

	cmd += `
	//end element
} catch(err) {
	`;

	if(step.defaultValue.validation.type === 'existence'){

		cmd += `}`;

		//end catch
		if(step.defaultValue.validation.data.value == 'exists'){
				cmd += `
				if(element == undefined) {
					return doneSync('${cbId}', exceptionMessage || 'Element not fount');
				}

				return doneSync('${cbId}');
				`;
		}else{
			cmd += `
			if(element != undefined) {
				return doneSync('${cbId}', exceptionMessage || 'Element should not be found');
			}

			return doneSync('${cbId}');
			`;
		}

		/*
		if(step.defaultValue.validation.data.value == 'notExists'){
			cmd += `
				return doneSync('${cbId}', '${step.defaultValue.exceptionMessage}' || 'Element not found');
			}
			`;
		}*/

	} else if(step.defaultValue.validation.type ===  'property'){

		//end catch
		cmd += `
			return doneSync('${cbId}', err);
		}
		`;

		cmd += `
		let val;
		try{
			val = await element.getAttribute('${step.defaultValue.validation.property}');
		}catch(e) {
			return doneSync('${cbId}', err);
		}
		`;

		switch(step.defaultValue.validation.comparator){
			case 'eq' :
			cmd += `
			if(val != '${text}') {
				return doneSync('${cbId}', exceptionMessage || 'Value not equals');
			}

			return doneSync('${cbId}');
			`;
			break;
			case 'neq' :
			cmd += `
			if(val == '${text}') {
				return doneSync('${cbId}', exceptionMessage || 'Value should not be equals');
			}

			return doneSync('${cbId}');
			`;
			break;
			case 'gt' :
				cmd += `
				let fval = parseFloat(val);
				if(isNaN(fval)) {
					return doneSync('${cbId}', 'The field value (' + val + ') is not a number.');
				}

				let compVal = '${text}';
				let comp = parseFloat(compVal);

				if(isNaN(comp)) {
					return doneSync('${cbId}', 'The field comparator (' + compVal + ') is not a number.');
				}

				if(fval < comp) {
					return doneSync('${cbId}', exceptionMessage || 'The value is not greather tham the comparator.');
				}

				return doneSync('${cbId}');
				`;
			break;
			case 'gte' :

				cmd += `
				let fval = parseFloat(val);
				if(isNaN(fval)) {
					return doneSync('${cbId}', 'The field value (' + val + ') is not a number.');
				}

				let compVal = '${text}';
				let comp = parseFloat(compVal);

				if(isNaN(comp)) {
					return doneSync('${cbId}', 'The field comparator (' + compVal + ') is not a number.');
				}

				if(fval <= comp) {
					return doneSync('${cbId}', exceptionMessage || 'The value is not greather or equals tham the comparator.');
				}

				return doneSync('${cbId}');
				`;

			break;
			case 'lt' :
				cmd += `
				val = parseFloat(val);
				if(isNaN(val)) {
					return doneSync('${cbId}', 'The field value (' + val + ') is not a number.');
				}

				let compVal = '${text}';
				let comp = parseFloat(compVal);

				if(isNaN(comp)) {
					return doneSync('${cbId}', 'The field comparator (' + compVal + ') is not a number.');
				}

				if(val > comp) {
					return doneSync('${cbId}', exceptionMessage || 'The value is not less tham the comparator.');
				}

				return doneSync('${cbId}');
				`;
			break;
			case 'lte' :

				cmd += `
				val = parseFloat(val);
				if(isNaN(val)) {
					return doneSync('${cbId}', 'The field value (' + val + ') is not a number.');
				}

				let compVal = '${text}';
				let comp = parseFloat(compVal);

				if(isNaN(comp)) {
					return doneSync('${cbId}', 'The field comparator (' + compVal + ') is not a number.');
				}

				if(val <= comp) {
					return doneSync('${cbId}', exceptionMessage || 'The value is not less or equals tham the comparator.');
				}

				return doneSync('${cbId}');
				`;
			break;
			case 'contains':
				cmd += `
				if(val.indexOf('${text}') == -1) {
					return doneSync('${cbId}', exceptionMessage || 'The field value does not contain "${text}"');
				}

				return doneSync('${cbId}');
				`;
			break;
		}
	}


	console.log(new Date().toLocaleString(),'[WORKER] - processScript', cmd);
	return cmd;

}

function processTypeAction(step, cbId, device, testSuite){
	console.log(new Date().toLocaleString(),'[WORKER] - processType', step, testSuite);

	let text = '';

	if (step.subType == 'static') {
		text = step.defaultValue.value;
	} else {
		text = testSuite.variables.find((v) => v.name === step.defaultValue.value).value;
	}

	switch(step.defaultValue.method){
		case 'relative':
			return `driver.keys('${text}', (err) => {
				doneSync('${cbId}', err);
			});`
		break;
		case 'xpath':
			return `driver.elementByXPath('${step.defaultValue.reference}', function(err, element){
				if(err){
					return doneSync('${cbId}', err);
				}else{
					element.type('${text}', function(err){
						return doneSync('${cbId}', err);
					});
				}
			});`
		break;
		case 'resource-id':
			return `driver.elementById('${step.defaultValue.reference}', function(err, element){
				if(err){
					return doneSync('${cbId}', err);
				}else{
					element.type('${text}', function(err){
						return doneSync('${cbId}', err);
					});
				}
			});`
		case 'name':
				return `driver.elementById('${step.defaultValue.reference}', function(err, element){
					if(err){
						return doneSync('${cbId}', err);
					}else{
						element.type('${text}', function(err){
							return doneSync('${cbId}', err);
						});
					}
				});`
		break;
	}
}

function processTouchAction(step, cbId, device) {
	console.log(new Date().toLocaleString(),'[WORKER] - processTouch', step, device._id);
	
	switch(step.subType){
		case 'relative':
			return `
				let ta = new TouchAction(driver);
				ta
				.press({x: ${Math.round(step.defaultValue.origin.x * device.model.resolution.width)}, y: ${Math.round(step.defaultValue.origin.y * device.model.resolution.height)}})
				.wait(${step.defaultValue.duration})
				.moveTo({x: ${Math.round(step.defaultValue.dest.x * device.model.resolution.width)}, y: ${Math.round(step.defaultValue.dest.y * device.model.resolution.height)}})
				.release();

				ta.perform((err) => doneSync('${cbId}', err));
				`;
			break;
			case 'element':
				switch(step.defaultValue.method){
					case 'name':
					case 'resource-id':
						return `
						driver.elementById('${step.defaultValue.reference}', (err, el) => {
							if (err) {
								return doneSync('${cbId}', err);
							}
							el.click((err) => {
								return doneSync('${cbId}', err);
							});
						});
						`;
					break;
					case 'xpath':
						return `
						driver.elementByXPath('${step.defaultValue.reference}', (err, el) => {
							if (err) {
								return doneSync('${cbId}', err);
							}
							el.click((err) => {
								return doneSync('${cbId}', err);
							});
						});
						`;
					break;
				}
			break;
	}
}

class Worker{
	constructor(token, testSuite, device, path) {
	  	this.token = token;
		this.testSuite = testSuite;
		this.position = -1;
		this.device = device;
		this.path = path;
		this.functional = new Functional();
		return this.setup().then(() => this.run());
	}

	processSnippet(step, cbId){
		console.log(new Date().toLocaleString(),'[WORKER] - processSnippet', step);

		let command;
		console.log(step);
		switch(step.type){
			case 'pressKey':
				command = `driver.pressKeycode('${step.defaultValue}', (err) => {
					doneSync('${cbId}', err);
				});`
			break;
			////TODO: ???
			case 'validate':
					command = processValidateAction(step, cbId, this.device, this.testSuite);
			break;
			case 'keyboard':
				command = processTypeAction(step, cbId, this.device, this.testSuite);
			break;
			case 'touchAction':
				command = processTouchAction(step, cbId, this.device, this.testSuite);
			break;
			case 'script':
				command = processScript(step, cbId, this.device, this.testSuite);
			break;
			case 'delay':
				command = `setTimeout(() => doneSync('${cbId}'), ${step.defaultValue * 1000});`
			break;
			default:
				command = `doneSync('${cbId}');`;
			break;
		}

		return command;
	}

	async executeStep(step){
		return new Promise(async (resolve, reject) => {
			/*this.worker.send({
				action: 'run',
				value: code,
			});*/
			console.log(new Date().toLocaleString(),'[WORKER] - processType', step);

			if(step.type === 'folder'){
				//// TODO:
				resolve();
			}else{

				let cbId = uuidv4();
				let code = this.processSnippet(step, cbId);

				//TODO impl timeout

				this.worker.once('response_'+cbId, (err, data) => {
					//this.worker.removeAllListeners();
					if(!err) {
						resolve();
					}else{
						reject(err);
					}
				});

				this.worker.once('done', () => {
					//this.worker.removeAllListeners();

					resolve();
				});

				this.worker.once('fail', (err) => {
					//this.worker.removeAllListeners();
					reject(err);
				});

				console.log('running:', code);
				this.worker.send({
					action: 'run',
					value: code,
				});
				//reject('spto');
			}

		});
	}

	run() {
		return new Promise(async (resolve, reject) => {
			let ref = this;
			async.eachSeries(this.testSuite.testCases, (testCase, cbCase) => {
				this.functional.startTestCase(testCase.name)
				.then(() => {
					return new Promise((resolve, reject) => {
						async.eachSeries(testCase.steps, (step, cbStep) => {
							if(step.alias && step.alias.length > 0) {
								ref.functional.startStep(step.alias).then(() => {
									return ref.executeStep(step);
								})
								.then(() => cbStep())
								.catch((e) => {
									console.log('catch step');
									return cbStep(e);
								});
							}else{
								ref.executeStep(step)
								.then(() => cbStep())
								.catch((e) => {
									console.log('catch step');
									return cbStep(e);
								});
							}

						}, (e) => {
							this.functional.end().then(() => {
								if(e){
									reject(e);
								}else{
									resolve();
								}
							});
						});
					})
				})
				.then(() => cbCase())
				.catch((e) => {
					return cbCase(e);
				})
			}, (e) => {
				let cbId = uuidv4();

				this.worker.once('response_' + cbId, (data) => {
					if(e){
						reject(e);
					}else{
						resolve();
					}
				});

				let reason = e ? JSON.stringify(e, null, "\t") : null;

				this.functional.quit(reason).then(async () => {

					await exec(`ps aux | grep ${this.device._id} | grep -v grep | awk '{print $2}' | xargs kill -9`);

					this.worker.send({
						action: 'quit',
						id: cbId
					});
				});

			});
		});
	}

	setup() {
		return new Promise( async (resolve, reject) => {
			this.position = -1;
			console.log(new Date().toLocaleString(),'[WORKER] - Setup');
			
			while(this.position !== 0) {
				let checkResp = await post('/services/device/ticket/check','application/x-www-form-urlencoded', querystring.stringify({
					token: this.token,
				}));
				checkResp = JSON.parse(checkResp);

				if(checkResp.message === 'Token Unrecognized'){
					return reject('Token Unrecognized');
				}else{
					console.log(new Date().toLocaleString(), 'Token and position:',this.token, checkResp.position);
					this.position = checkResp.position;
					//this.device = checkResp.device;
					await this.sleep(5000);
					//break;
				}
			}

			cluster.setupMaster({
				exec: './runner.js',
				args: [JSON.stringify({
					device: this.device,
					token: this.token,
					testSuite: this.testSuite,
					path: this.path,
				})],
				//silent: true,
			});

			this.worker = cluster.fork();

			this.worker.on('message', (data) => {
				console.log(new Date().toLocaleString(),'[WORKER] - Message:', data.action, data.value);
				
				try {
					switch (data.action) {
					case 'console':
						//TODO
						break;
					case 'started':
						this.functional.setup(data.value);
						resolve();
						break;
					case 'set-var':
						let index = this.testSuite.variables.findIndex((variable) => variable.name == data.key);
						if(index >= 0){
							this.testSuite.variables[index].value = data.value;
						}
					break;
					case 'startup-fail':
						console.error(data.value);
						reject('Failed to start session, check the startup params');
						break;
					case 'done-sync':
						this.worker.emit('response_' + data.id, data.err, data.data);
					break;
					case 'done':
						this.worker.emit('done');
					break;
					case 'fail':
						this.worker.emit('fail', data.err);
					break;
					default:
						this.worker.emit('response_' + data.id, data.value);
						break;
					}
				} catch (e) {
					console.log(e);
					return reject('Unrecognized action');
				}
			});


		});
	}

	sleep (duration){
		return new Promise((resolve) => {
			setTimeout(resolve, duration);
		});
	}
}
module.exports = Worker;
