import amqp from 'amqplib/callback_api';
import Worker from './worker';

global.config = require('../config/default.json');

amqp.connect(global.config.mq, function(err, conn) {

	if (err) {
		console.error(err);
		return;
		//return res.status(500).send('Failed to run test suite');
	}

	conn.createChannel(function(err, ch) {
		console.log(new Date().toLocaleString(),'[INDEX] - Conexão com fila MQ OK');
		let q = global.config.queue;

		ch.prefetch(100);
		ch.consume(q, async function(msg) {
			//var secs = msg.content.toString().split('.').length - 1;

			/*const waitFor = (ms) => new Promise(r => setTimeout(r, ms));

			await waitFor(Math.random() * (10000 - 1000) + 1000)*/

			try{
				let data = JSON.parse(msg.content.toString());
				console.log(new Date().toLocaleString(),'[INDEX] - dados recebidos da fila:');
				console.log(data);
				let worker = new Worker(data.token, data.testSuite, data.device, data.path).then(() => {
					console.log('done');
					ch.ack(msg);
				}).catch((e) => {
					console.log('catch', e);
					ch.ack(msg);
				});

				/*setTimeout(function() {
					console.log(" [x] Done");
					ch.ack(msg);
					//ch.reject(msg);
					//ch.nack(msg);
				}, 10 * 1000);*/

			}catch(e){
				console.log(e);
				ch.ack(msg);
			}
		}, {noAck: false});
	});
});
