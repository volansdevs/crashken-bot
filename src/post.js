global.config = require('../config/default.json');

if(config.crashken.protocol == 'http'){
	const http = require('http');
} else {
	const http = require('https');
}

module.exports = function post(path, type, fields){
	return new Promise((resolve, reject) => {
		let postData = fields;

		let options = {
			hostname: global.config.crashken.host,
		 	port: global.config.crashken.port,
			path: path,
			method: 'POST',
			headers: {
		    'Content-Type': type,
		    'Content-Length': Buffer.byteLength(postData)
		  }
		};

		const req = http.request(options, (res) => {
			let output = '';
		  res.setEncoding('utf8');
		  res.on('data', (chunk) => {
				output += chunk;
		  });
		  res.on('end', () => {
				resolve(output);
		  });
		});

		req.on('error', (e) => {
			reject(e);
		});

		req.write(postData);
		req.end();
	});
}
